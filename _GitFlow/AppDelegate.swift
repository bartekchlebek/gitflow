//
//  AppDelegate.swift
//  _GitFlow
//
//  Created by Bartek Chlebek on 31.08.2014.
//  Copyright (c) 2014 Bartek Chlebek. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(application: UIApplication!, didFinishLaunchingWithOptions launchOptions: NSDictionary!) -> Bool {
    // Override point for customization after application launch.
    var window = UIWindow(frame: UIScreen.mainScreen().bounds)
    var NC = UINavigationController(rootViewController: ViewController());
    window.rootViewController = NC;
    window.makeKeyAndVisible();
    self.window = window;
    return true
  }
}

